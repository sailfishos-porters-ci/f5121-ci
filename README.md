# Sailfish OS on Xperia X (f5121)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
2.2.1.18 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/f5121-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/5121-ci/commits/master)



